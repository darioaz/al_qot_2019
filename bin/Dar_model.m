function [NLIN_var_inter, NLIN_var_intra1] = Dar_model(alpha, Nspan, L, PdBm, BaudRate, ChSpacing, M, ModFormats)
%
% Computes an estimate of the nonlinear interference power
%
% Function inputs:
%
% alpha : Fiber loss coefficient [dB/km]
% Nspan: Number of spans, assumed all equal
% L :  Span length [km]
% PdBm : input power per channel [dBm]
% BaudRate : Baud-Rate [GHz]
% ChSpacing: Channel spacing [GHz]. A matrix of size [Nfreq, 2], where Nfreq is
%            the number of different central frequencies in the flexi-grid.
%            For each row in the matrix, we have 2 entries: the first entry
%            is the distance (in GHz) of the n-th central frequency to the
%            central frequency of the channel of interest; the second entry
%            is the number of channels with the same channel distance
%            (either 1 or 2)% NChLeft : Number of adjacent channels to the left part of the spectrum
% M : Modulation format of the channel of interest. 



%% System parameters

%clear;clc;tic;

PolMux = 1;     % 0 when single polarization, 1 with polarization multiplexing

gamma = 1.3;    % Nonlinearity coefficient in [1/W/km]

beta2 = 21;     % Dispersion coefficient [ps^2/km]

%alpha = 0.2;    % Fiber loss coefficient [dB/km]

%Nspan = 5;      % Number of spans

%L = 100;        % Span length [km]

PD = 0;         % Pre-dispersion [ps^2]

%PdBm = -2;      % Average input power [dBm]

N = 1000000;    % Number of integration points in algorithm [14]. Should
                % be set such that the relative error is desirably small.

%% Monte-Carlo integration

alpha_norm = alpha/10*log(10);

T=1000/BaudRate;

P0 = 10^((PdBm-30)/10);

beta2_norm = beta2/T^2;

PD_norm = PD/T^2;


%% Nonlinear interference contributions calculation
    
NLIN_var_inter = 0;
                
%Ncontributions = max(NChLeft, NChRight); % number of nonlinear noise contributions
%Nboth = max(NChLeft, NChRight);

[kur, kur3] = kurtosis_constellation(M);
%kur = 2; %Gauss
%kur3 = 6; %Gauss

for neighbour = 1:length(ChSpacing)
   

    %BaudRate = 32;  % Baud-rate [GHz]
    
    %ChSpacing = 50; % Channel spacing [GHz]
    
    %kur = 1.32;     % Second order modulation factor <|a|^4>/<|a|^2>^2
    
    %kur3 = 1.96;    % Third order modulation factor <|a|^6>/<|a|^2>^3
    
    
    ChSpacing_norm = ChSpacing(neighbour)./BaudRate;

    
    [kur_interf, kur3_interf] = kurtosis_constellation(ModFormats(neighbour));
    
    
    %% calculate inter-channel nonlinear noise variance according to Eq. (1)
    
    [NLIN_var chi1 chi2 Err] = calc_interChannel(gamma,beta2_norm,alpha_norm,...
        Nspan,L,PD_norm,P0,kur_interf,kur3_interf,N,PolMux,ChSpacing_norm);
    
    %if neighbour <= Nboth
        NLIN_var_inter = NLIN_var_inter+ NLIN_var*1000; % in [mW]
    %else
    %    NLIN_var_inter = NLIN_var_inter+ NLIN_var*1000; % in [mW]
    %end
    
    %% display
    
    % disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    %
    % if(PolMux == 1)
    %
    %     disp('%%%Polarization Multiplexed case is considered%%%');
    %
    % end
    %
    % disp('%%Results correspond to a single interferer%%%')
    %
    % disp(['(1) chi_1 = ' num2str(chi1) ', chi_2 = ' num2str(chi2)]);
    %
    % disp(['(2) NLIN variance according to Eq. (1) is ' num2str(NLIN_var)...
    %         ' Watts (' num2str(10*log10(NLIN_var*1000)) 'dBm).'...
    %         ' Relative computation error is ', num2str(Err*100),'%']);
    %

end
   

%% calculate the contribution of the additional inter-channel terms of [3]

% NLIN_var_addTerm = calc_interChannel_addTerms(gamma,beta2_norm,alpha_norm,...
% Nspan,L,PD_norm,P0,kur,kur3,N,PolMux,ChSpacing_norm);

 

%% calculate intra-channel nonlinear noise

%% NLIN_var_intra(1) is the intra-channel nonlinear noise variance

%% NLIN_var_intra(2) is due to the nonlinear broadening of the adjacent interferer

NLIN_var_intra = calc_intraChannel(gamma,beta2_norm,alpha_norm,...
  Nspan,L,PD_norm,P0,kur,kur3,N,PolMux,0);

NLIN_var_intra1 = NLIN_var_intra(1)*1000; % in [mW]

%NLIN_var_addTerm = NLIN_var_addTerm + NLIN_var_intra(2);

 

%%Continue display

% disp(['(3) Contribution of additional inter-channel interference '...
%         'terms of [7] is ' num2str(NLIN_var_addTerm) ' Watts ('...
%         num2str(10*log10(NLIN_var_addTerm*1000)) 'dBm)']);
% 
% disp(['(4) Total (inter-channel) NLIN variance (2)+(3) is ' num2str(NLIN_var+NLIN_var_addTerm)...
%         ' Watts (' num2str(10*log10((NLIN_var+NLIN_var_addTerm)*1000)) 'dBm)']);
% 
% disp(['(5) Intra-Channel nonlinear noise variance is ' num2str(NLIN_var_intra(1))...
%         ' Watts (' num2str(10*log10(NLIN_var_intra(1)*1000)) 'dBm)']);
% 
% toc;
% 
% disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
