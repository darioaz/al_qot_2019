function [kur, kur3] = kurtosis_constellation(M)
% Computes normalized moments of the constellation
%
% M : modulation format

const = qammod(0:M-1, M);

Power = mean(abs(const).^2);

const = const./sqrt(Power);

kur = mean(abs(const).^4)/(mean(abs(const).^2))^2;

kur3 = mean(abs(const).^6)/(mean(abs(const).^2))^3;
