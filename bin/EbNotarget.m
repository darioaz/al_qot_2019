function target = EbNotarget(M, BERtarget)
%
% M: constellation points
% BERtarget: BER target
%
% target: Eb/No target in dB

variable_ebno = ['ebno' num2str(M) 'QAM'];
variable_ber = ['ber' num2str(M) 'QAM'];

load('ber_vs_EbNo_QAM.mat', variable_ebno, variable_ber)

ebno = eval(variable_ebno);
ber = eval(variable_ber);

[~, I] = min(abs(ber-BERtarget));
target = ebno(I);