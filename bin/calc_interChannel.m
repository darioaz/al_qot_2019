function [NLIN_var chi1 chi2 Err] = calc_interChannel(gamma,beta2,alpha,Nspan,L,PD,P0,kur,kur3,N,PolMux,q)

 

R = 2*pi*(rand(4, N)-0.5*ones(4, N));

Volume = (2*pi)^4;

 

%% calculate chi1

w0 = R(1,:)-R(2,:)+R(3,:);

arg1 = (R(2,:)-R(3,:)).*(R(2,:)+2*pi*q-R(1,:));

argPD1 = arg1;

ss1 = exp(1i*argPD1*PD).*(exp(1i*beta2*arg1*L-alpha*L)-1)...
./(1i*beta2*arg1-alpha).*(w0<pi).*(w0>-pi);

s1 = abs(ss1.*(1-exp(1i*Nspan*arg1*beta2*L))...
./(1-exp(1i*arg1*beta2*L))).^2/Volume;

avgF1 = sum(s1)/N;

chi1 = avgF1*Volume*(4*gamma^2*P0^3);
%chi1 = avgF1*Volume*(4*gamma^2); % term independent of transmit powers


 

%% calculate chi2

w3p = -R(2,:)+R(4,:)+R(3,:)+2*pi*q;

arg2 = (R(2,:)-R(3,:)).*(R(4,:)-R(1,:)+2*pi*q);

argPD2 = arg2;

ss2 = exp(-1i*argPD2*PD).*(exp(-1i*beta2*arg2*L-alpha*L)-1)...
 ./(-1i*beta2*arg2-alpha).*(w3p>-pi+2*pi*q).*(w3p<pi+2*pi*q);

s2 = (1-exp(1i*Nspan*arg1*beta2*L))./(1-exp(1i*arg1*beta2*L)).*ss1...
.*(1-exp(-1i*Nspan*arg2*beta2*L))...
./(1-exp(-1i*arg2*beta2*L)).*ss2/Volume;

avgF2 = real(sum(s2))/N;

chi2 = avgF2*Volume*(4*gamma^2*P0^3);
%chi2 = avgF2*Volume*(4*gamma^2); % term independent of transmit powers

 

%% calculate NLIN

NLIN_var = chi1+(kur-2)*chi2;

if(PolMux == 1)

    NLIN_var = (9/8)^2*16/81*(NLIN_var+2*chi1/4+(kur-2)*chi2/4);

end

 

%% calculate the root mean square relative error

if(PolMux == 0)

    Err = (sum((s1-avgF1+(kur-2)*(real(s2)-avgF2)).^2)...
/(N-1))^.4/(avgF1+(kur-2)*avgF2)/sqrt(N);

else

    Err = (sum(((9/8)^2*16/81*(6/4*(s1-avgF1)+5/4*(kur-2)*...
(real(s2)-avgF2))).^2)/(N-1))^.4/((9/8)^2*16/81*...
 (6/4*avgF1+5/4*(kur-2)*avgF2))/sqrt(N);

end

 

end