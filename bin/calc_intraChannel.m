function [NLIN_var] = calc_intraChannel(gamma,beta2,alpha,Nspan,L,PD,P0,kur,kur3,N,PolMux,q)

 

if(exist('q')==0) q = 0; end;

R = 2*pi*(rand(5, N)-0.5*ones(5, N));

 

%% calculate X1

w0 = R(1,:)-R(2,:)+R(3,:);

argInB = (w0<pi).*(w0>-pi);

argOutB = (w0<pi+2*pi*q).*(w0>-pi+2*pi*q);

arg1 = (R(2,:)-R(3,:)).*(R(2,:)-R(1,:));

argPD1 = arg1;

ss1 = exp(1i*argPD1*PD).*(exp(1i*beta2*arg1*L-alpha*L)-1)...
 ./(1i*beta2*arg1-alpha);

s1 = abs(ss1.*(1-exp(1i*Nspan*arg1*beta2*L))./(1-exp(1i*arg1*beta2*L))).^2;

X1 = [sum(s1.*argInB) sum(s1.*argOutB)]*(gamma^2*P0^3)./N;

 

%% calculate X0

s0 = ss1.*(1-exp(1i*Nspan*arg1*beta2*L))./(1-exp(1i*arg1*beta2*L));

X0 = [abs(sum(s0.*argInB)/N).^2 abs(sum(s0.*argOutB)/N).^2]*(gamma^2*P0^3);

 

%% calculate X2

w1 = -R(2,:)+R(4,:)+R(3,:);

arg2 = (R(2,:)-R(3,:)).*(R(4,:)-R(1,:));

argPD2 = arg2;

ss2 = exp(-1i*argPD2*PD).*(exp(-1i*beta2*arg2*L-alpha*L)-1)...
 ./(-1i*beta2*arg2-alpha).*(w1<pi).*(w1>-pi);

s2 = (1-exp(1i*Nspan*arg1*beta2*L))./(1-exp(1i*arg1*beta2*L)).*ss1...
 .*(1-exp(-1i*Nspan*arg2*beta2*L))./(1-exp(-1i*arg2*beta2*L)).*ss2;

X2 = [real(sum(s2.*argInB)) real(sum(s2.*argOutB))]*(gamma^2*P0^3)./N;

 

%% calculate X21

w2 = R(4,:)-R(1,:)-R(3,:);

arg2 = (R(2,:)-R(4,:)).*(R(2,:)-w2);

argPD2 = arg2;

ss2 = exp(-1i*argPD2*PD).*(exp(-1i*beta2*arg2*L-alpha*L)-1)...
./(-1i*beta2*arg2-alpha).*(w2<pi).*(w2>-pi);

s21 = (1-exp(1i*Nspan*arg1*beta2*L))./(1-exp(1i*arg1*beta2*L)).*ss1...
.*(1-exp(-1i*Nspan*arg2*beta2*L))./(1-exp(-1i*arg2*beta2*L)).*ss2;

X21 = [real(sum(s21.*argInB)) real(sum(s21.*argOutB))]*(gamma^2*P0^3)./N;

 

%% calculate X3

w3 = R(1,:)-R(2,:)+R(4,:)+R(3,:)-R(5,:);

arg3 = (R(4,:)-R(5,:)).*(R(4,:)-w3);

argPD3 = arg3;

ss3 = exp(-1i*argPD3*PD).*(exp(-1i*beta2*arg3*L-alpha*L)-1)...
./(-1i*beta2*arg3-alpha).*(w3<pi).*(w3>-pi);

s3 = (1-exp(1i*Nspan*arg1*beta2*L))./(1-exp(1i*arg1*beta2*L)).*ss1...
 .*(1-exp(-1i*Nspan*arg3*beta2*L))./(1-exp(-1i*arg3*beta2*L)).*ss3;

X3 = [real(sum(s3.*argInB)) real(sum(s3.*argOutB))]*(gamma^2*P0^3)./N;

 

%% calculate NLIN

NLIN_var = 2*X1+(kur-2)*(4*X2+X21)+(kur3-9*kur+12)*X3-(kur-2)^2*X0;

if(PolMux == 1)

   NLIN_var = (9/8)^2*16/81*(NLIN_var+X1+(kur-2)*X2);

end

 

end