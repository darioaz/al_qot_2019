function sen = sensitivity(M, BERtarget, DataRate)
% Compute the sensitivity for a given QAM constellation, BER target, and
% overall target data rate in two polarizations.
%
% M : constellation size (PolMux)
% BERtarget : BER target
% DataRate: Data rate target in Gb/s
%
% sen : required OSNR [dB] at Rx to meet uncoded BER target

Ebnotarget = EbNotarget(M, BERtarget);

Bref = 12.5; % reference bandwidth in GHz (corresponding to 0.1 nm)

sen = Ebnotarget+10*log10(log2(M))+10*log10(DataRate/(2*log2(M))/Bref);