function [NLIN_var] = calc_interChannel_addTerms(gamma,beta2,alpha,Nspan,L,PD,P0,kur,kur3,N,PolMux,q)

 

R = 2*pi*(rand(4, N)-0.5*ones(4, N));

 

%% calculate X21

w0 = R(1,:)-R(2,:)+R(3,:)+2*pi*q;

arg1 = (R(2,:)-R(3,:)-2*pi*q).*(R(2,:)-R(1,:));

argPD1 = arg1;

ss1 = exp(1i*argPD1*PD).*(exp(1i*beta2*arg1*L-alpha*L)-1)...
 ./(1i*beta2*arg1-alpha).*(w0<pi).*(w0>-pi);

s1 = abs(ss1.*(1-exp(1i*Nspan*arg1*beta2*L))./(1-exp(1i*arg1*beta2*L))).^2;

X21 = sum(s1)*(gamma^2*P0^3)/N;

 

%% calculate X22

w1 = R(1,:)-R(2,:)+R(4,:);

arg2 = (w1-R(3,:)-2*pi*q).*(R(2,:)-R(1,:));

argPD2 = arg2;

ss2 = exp(-1i*argPD2*PD).*(exp(-1i*beta2*arg2*L-alpha*L)-1)...
./(-1i*beta2*arg2-alpha).*(w1<pi).*(w1>-pi);

s2 = (1-exp(1i*Nspan*arg1*beta2*L))./(1-exp(1i*arg1*beta2*L)).*ss1...
.*(1-exp(-1i*Nspan*arg2*beta2*L))./(1-exp(-1i*arg2*beta2*L)).*ss2;

X22 = real(sum(s2))*(gamma^2*P0^3)/N;

 

%% calculate X23

w2 = R(1,:)+R(2,:)-R(3,:)-2*pi*q;

arg1 = (R(3,:)+2*pi*q-R(2,:)).*(R(3,:)+2*pi*q-R(1,:));

argPD1 = arg1;

ss3 = exp(1i*argPD1*PD).*(exp(1i*beta2*arg1*L-alpha*L)-1)...
./(1i*beta2*arg1-alpha).*(w2<pi).*(w2>-pi);

s3 = abs(ss3.*(1-exp(1i*Nspan*arg1*beta2*L))./(1-exp(1i*arg1*beta2*L))).^2;

X23 = sum(s3)*(gamma^2*P0^3)/N;

 

%% calculate X24

w3 = R(1,:)-R(4,:)+R(2,:);

arg2 = (R(3,:)+2*pi*q-R(4,:)).*(R(3,:)+2*pi*q-w3);

argPD2 = arg2;

ss4 = exp(-1i*argPD2*PD).*(exp(-1i*beta2*arg2*L-alpha*L)-1)...
./(-1i*beta2*arg2-alpha).*(w3<pi).*(w3>-pi);

s4 = (1-exp(1i*Nspan*arg1*beta2*L))./(1-exp(1i*arg1*beta2*L)).*ss3...
.*(1-exp(-1i*Nspan*arg2*beta2*L))./(1-exp(-1i*arg2*beta2*L)).*ss4;

X24 = real(sum(s4))*(gamma^2*P0^3)/N;

 

%% calculate NLIN

NLIN_var = 4*X21+4*(kur-2)*X22+2*X23+(kur-2)*X24;

if(PolMux == 1)

    NLIN_var = (9/8)^2*16/81*(NLIN_var+2*X21+(kur-2)*X22+X23+0*(kur-2)*X24);

end

 

end