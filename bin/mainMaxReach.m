%% Main program to compute MaxReach
NF = 5;
loss = 0.2;
BERtarget = 3e-4;
System_margin = 2;
AmplifierSpacing = 100;
neigh_distance = 12.5*[1 1]; % in GHz, the minimum is 1 slice
neigh_width = 10*[1 1];
neigh_constellation = 64*[1 1]; % worst modulation format

Reach = zeros(6, 10); % 6 modulation formats, and 10 traffic volumes

indM = 0;
for M = [2 4 8 16 32 64]
    indM = indM + 1;
    indD = 0;
    for DataRate = 1:10 % from 50 Gb/s to 500 Gb/s
        indD = indD + 1;
        Reach(indM, indD) = MaxReach(M, DataRate, NF, loss, BERtarget, System_margin, AmplifierSpacing, neigh_distance, neigh_width, neigh_constellation);
        disp(['Mod: ' num2str(M) '; DataRate: ' num2str(DataRate*50) '; Reach: ' num2str(Reach(indM, indD))])
        if Reach(indM, indD) == 0
            break
        end
    end
end

save('MaxReach.mat', 'Reach')