function  Reach = MaxReach(M, DataRate, NF, loss, BERtarget, System_margin, AmplifierSpacing, neigh_distance, neigh_width, neigh_constellation)
%
% Computes an estimate of the maximum reach to meet BERtarget
% 
% Assumption: The amplifier spacing determines the optimal input power,
% according to Fig. 9 of [1]
%
% M :        constellation size (choose among M=2,4,8,16,32,64)
% DataRate:  Data Rate (in 2 polarizations) to transmit, expressed in units. 
%            Each unit corresponds to 50 Gb/s
% NF :       Noise figure of the amplifier [dB] (assumed equal for each
%            span) min. 3 dB -- 7 dB
% loss :     Fiber losses [dB/km], typically 0.2 dB/km
% BERtarget: Target BER (typically 4e-3)
% System_margin: Typically 2 dB
% AmplifierSpacing : Distance between two adjacent amplifiers, in km
% neigh_distance : left and right guardband with separating the considered
%                  superchannel from the left and right neighbors (worst case among all
%                  links)
% neigh_width : number of transceivers in the nearest left and right
%               superchannels (worst case among all links)
% neigh_constellation : constellation (2,4,8,16,32,64) used in the nearest left and right
%                       superchannels (worst case among all links)
%
%
% Ref. [1] G. Bosco et al., "On the performance of Nyquist-WDM Terabit
% Superchannels based on PM-BPSK, PM-QPSK, PM-8QAM or PM-16QAM
% subcarriers", JLT 2011.
% 

DataRate = DataRate * 50; % Conversion from units to Gb/s
BaudRate = 28; % in GHz

% Number of required trasponders
N_txp = ceil(DataRate / (2*log2(M)*25));

% LossesSpans_dB = [];
% for i = 1:length(Lspans)
%     if mod(Lspans(i),AmplifierSpacing) == 0
%         LossesSpans_dB = [LossesSpans_dB AmplifierSpacing*loss*ones(1, Lspans(i)/AmplifierSpacing)];
%     else     
%         LossesSpans_dB = [LossesSpans_dB AmplifierSpacing*loss*ones(1, floor(Lspans(i)/AmplifierSpacing)) (Lspans(i)-AmplifierSpacing*floor(Lspans(i)/AmplifierSpacing))*loss];
%     end
% end
% 
% LossesSpans = 10.^(LossesSpans_dB/10); % Losses in each amplifier span, from dB to linear


Pin = (AmplifierSpacing*loss -22)/3; % optimal input power per channel, in dBmW, from [1]. Assumptions: all channels transmit
% at the same power Pin, assuming that all modulation formats are Gaussian.
% Conservative estimate.

Bref = 12.5; % reference bandwidth in GHz (@ 0.1 nm)

Implementation_penalty = 2*(M <= 4) + 3*(M>4);
%System_margin = 2; % 2 dB of system margin

Losses_50GHz = [0.05 0.07 0.13 0.44 0.7 1.44]; % from Table I of [1], last two values extrapolated with exponential fitting
Losses_33GHz = [0.18 0.25 0.31 0.65 0.84 1.26]; % from Table I of [1], last two values extrapolated with exponential fitting

B2B_penalty([2 4 8 16 32 64]) =  (Losses_50GHz-Losses_33GHz )/(50-33.3)*(37.5-33.3)+Losses_33GHz; % Linear interpolation to find value at 37.5 GHz


AdditionalPenalties = Implementation_penalty + System_margin + B2B_penalty(M);

%Nspans = ceil(sum(Lspans)/AmplifierSpacing);

max_reach = zeros(1, ceil(N_txp/2));

for n_txp = 1:ceil(N_txp/2)
        
    ChSpacingTot = [neigh_distance(1)+37.5*(1:neigh_width(1))+37.5*(n_txp-1), 37.5*(1:n_txp-1),37.5*(1:(N_txp-n_txp)), 37.5*(N_txp-n_txp)+neigh_distance(2)+37.5*(1:neigh_width(2))];
    %ChSpacing = [neigh_distance(1)+37.5*(1:neigh_width(1))+37.5*(n_txp-1),neigh_distance(1)+37.5*neigh_width(1)+37.5*(n_txp-1)+ 37.5*(1:n_txp-1),neigh_distance(1)+37.5*neigh_width(1)+37.5*(n_txp-1)+ 37.5*(1:(N_txp-n_txp)), neigh_distance(1)+37.5*neigh_width(1)+37.5*(n_txp-1)+37.5*(N_txp-n_txp)+neigh_distance(2)+37.5*(1:neigh_width(2))];
    ModFormatsTot = [neigh_constellation(1)*ones(1,neigh_width(1)), M*ones(1,N_txp-1), neigh_constellation(2)*ones(1,neigh_width(2))];

    ChSpacing = ChSpacingTot(ChSpacingTot <= 150);  % we consider only the nonlinear contribution of channels closer than 100 GHz
    ModFormats = ModFormatsTot(ChSpacingTot <= 150);
        
    % tentative nonlinear penalty
    Nonlinear_penalty = 2.5; % in dB
    
    OSNRtarget = sensitivity(M, BERtarget, DataRate) ;

    LossesSpans = 58 + Pin - NF - OSNRtarget - Implementation_penalty - System_margin - Nonlinear_penalty - B2B_penalty(M); % in dB
    NSpans = 10^((LossesSpans - (AmplifierSpacing*loss))/10);
    
    for Nspans_tentative = floor(NSpans)+(0:100)
        Nonlinear_penalty = nonlinearpenalty(BaudRate,loss, Nspans_tentative, AmplifierSpacing, Pin, M, DataRate, ChSpacing, ModFormats, NF, AdditionalPenalties);
        
        OSNRaccumulation = 58 + Pin - NF - 10*log10(Nspans_tentative)-AmplifierSpacing*loss - Implementation_penalty - System_margin - Nonlinear_penalty - B2B_penalty(M);
      
%         % Compute BER at the receiver
%         EbNo = OSNRaccumulation-10*log10(log2(M))-10*log10(BaudRate/Bref);
%         BER = BERcalculation(EbNo, M);
        if OSNRaccumulation < OSNRtarget
            break
        end
    end
    
    max_reach(n_txp) = (Nspans_tentative-1)*AmplifierSpacing;
end

Reach = min(max_reach); % worst MaxReach among all transceivers of the superchannel