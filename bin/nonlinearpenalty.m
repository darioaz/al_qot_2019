function Nonlinear_penalty = nonlinearpenalty(BaudRate, loss, Nspan, L, Pin, M, DataRate, ChSpacing, ModFormats, NF, AdditionalPenalties)
% Computes the nonlinear penalty

BERtarget = 4e-3; % we need it to assess the nonlinear penalty. The nonlinear penalty depends on the BERtarget.

OSNRtarget = sensitivity(M, BERtarget, DataRate); % We convert BERtarget into an OSNRtarget in [dB]

[NLIN_var_inter, NLIN_var_intra1] = Dar_model(loss, Nspan, L, Pin, BaudRate, ChSpacing, M, ModFormats);


%var_ASE = 10^((-58 + NF + L*loss + 10*log10(Nspan))/10);

var_NLIN = NLIN_var_inter+ NLIN_var_intra1; % in [mW]

temp = 10^((Pin  - AdditionalPenalties - OSNRtarget)/10) - var_NLIN;

if temp <= 0
    Nonlinear_penalty = 50;
else

    NmaxdB = 10*log10(10^((Pin  - AdditionalPenalties - OSNRtarget)/10) - var_NLIN)  + 58 - NF - loss*L;

    Lmax = NmaxdB + 10*log10(L);

    Pin_LinSyst = OSNRtarget + AdditionalPenalties - 58 + NF + loss*L + NmaxdB; %in [dBm]

    Nonlinear_penalty = Pin - Pin_LinSyst;
end