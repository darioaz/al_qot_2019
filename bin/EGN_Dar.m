function Popt = Dar_model(alpha, Nspan, L, Pinvec, BaudRate, ChSpacing, ModFormat, ModFormatvec)
%
% Computes an estimate of the optimal input power
%
% Function inputs:
%
% alpha : Fiber loss coefficient [dB/km]
% Nspan: Number of spans, assumed all equal
% L :  Span length [km]
% Pinvec : vector of input powers of all neighbour channels. The first two
%          values are the maximum transmit powers of the closest channels along the lightpath,
%          the next 2 values are the maximum transmit powers of the second closest channels, etc...
% BaudRate : Baud-Rate [GHz]
% ChSpacing : Channel spacing [GHz]
% ModFormat : Modulation format of the channel of interest. Choose among
%             2,4,16,32,64 QAM
% ModFormatvec : vector of modulation formats of all neighbour channels.
%               The first two values are the largest constellation sizes of
%               the closest channels along the lightpath, the next 2 values
%               are the largest constellation sizes of
%               the second closest channels, etc...



%% System parameters

%clear;clc;tic;

PolMux = 1;     % 0 when single polarization, 1 with polarization multiplexing

gamma = 1.3;    % Nonlinearity coefficient in [1/W/km]

beta2 = 21;     % Dispersion coefficient [ps^2/km]

alpha = 0.2;    % Fiber loss coefficient [dB/km]

Nspan = 5;      % Number of spans

L = 100;        % Span length [km]

PD = 0;         % Pre-dispersion [ps^2]

PdBm = -2;      % Average input power [dBm]

BaudRate = 32;  % Baud-rate [GHz]

ChSpacing = 50; % Channel spacing [GHz]

kur = 1.32;     % Second order modulation factor <|a|^4>/<|a|^2>^2

kur3 = 1.96;    % Third order modulation factor <|a|^6>/<|a|^2>^3

N = 1000000;    % Number of integration points in algorithm [14]. Should

                % be set such that the relative error is desirably small.

 

%% Monte-Carlo integration

alpha_norm = alpha/10*log(10);

T=1000/BaudRate;

P0 = 10^((PdBm-30)/10);

beta2_norm = beta2/T^2;

PD_norm = PD/T^2;

ChSpacing_norm = ChSpacing./BaudRate;

 

%% calculate inter-channel nonlinear noise variance according to Eq. (1)

[NLIN_var chi1 chi2 Err] = calc_interChannel(gamma,beta2_norm,alpha_norm,...
                            Nspan,L,PD_norm,kur,kur3,N,PolMux,ChSpacing_norm);

 

%% display

disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');

if(PolMux == 1)

    disp('%%%Polarization Multiplexed case is considered%%%');

end

disp('%%Results correspond to a single interferer%%%')

disp(['(1) chi_1 = ' num2str(chi1) ', chi_2 = ' num2str(chi2)]);

disp(['(2) NLIN variance according to Eq. (1) is ' num2str(NLIN_var)...
        ' Watts (' num2str(10*log10(NLIN_var*1000)) 'dBm).'...
        ' Relative computation error is ', num2str(Err*100),'%']);

   

%% calculate the contribution of the additional inter-channel terms of [3]

NLIN_var_addTerm = calc_interChannel_addTerms(gamma,beta2_norm,alpha_norm,...
Nspan,L,PD_norm,P0,kur,kur3,N,PolMux,ChSpacing_norm);

 

%% calculate intra-channel nonlinear noise

%% NLIN_var_intra(1) is the intra-channel nonlinear noise variance

%% NLIN_var_intra(2) is due to the nonlinear broadening of the adjacent interferer

NLIN_var_intra = calc_intraChannel(gamma,beta2_norm,alpha_norm,...
  Nspan,L,PD_norm,P0,kur,kur3,N,PolMux,ChSpacing_norm);

NLIN_var_addTerm = NLIN_var_addTerm + NLIN_var_intra(2);

 

%%Continue display

disp(['(3) Contribution of additional inter-channel interference '...
        'terms of [7] is ' num2str(NLIN_var_addTerm) ' Watts ('...
        num2str(10*log10(NLIN_var_addTerm*1000)) 'dBm)']);

disp(['(4) Total (inter-channel) NLIN variance (2)+(3) is ' num2str(NLIN_var+NLIN_var_addTerm)...
        ' Watts (' num2str(10*log10((NLIN_var+NLIN_var_addTerm)*1000)) 'dBm)']);

disp(['(5) Intra-Channel nonlinear noise variance is ' num2str(NLIN_var_intra(1))...
        ' Watts (' num2str(10*log10(NLIN_var_intra(1)*1000)) 'dBm)']);

toc;

disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
