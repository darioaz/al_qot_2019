function BER = BERcalculation(EbNo, M)
%
% Compute the BER
%
% EbNo: Eb/N0 in [dB]
% M :   QAM Constellation size

variable_ebno = ['ebno' num2str(M) 'QAM'];
variable_ber = ['ber' num2str(M) 'QAM'];

load('ber_vs_EbNo_QAM.mat', variable_ebno, variable_ber)

ebno = eval(variable_ebno);
ber = eval(variable_ber);

[~, I] = min(abs(ebno-EbNo));
BER = ber(I);