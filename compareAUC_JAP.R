## Japan network topology
# Create a comparison of AUC over kernels and with random selection
rm(list=ls())

library(DiceKriging)
library(KrigInv)
library(readxl)
library(dplyr)
library(ggplot2)
library(tidyr)


source("./bin/AuxiFuns.R")

## Experiment ID
# here the completed experiments are save by task number and ber_number
# task_number -- the task number in log_experiments
## 

all_tsk<-seq(1,12)

tsk_fin <- rep(400,length(all_tsk))
fin_it<-400

res_rand_auc<- 10

fin_it_rand <- fin_it/res_rand_auc+1

# folders
folderPlots <- paste("./Plots/training_1/",sep="")

all_exp1<- read.table("./bin/exp_jap.txt")

sizeAUC <- max(all_exp1$n_iter[all_tsk])+1
all_AUC <- data.frame(matrix(NA,ncol=8,nrow=length(all_tsk)*(2*2*sizeAUC)))
colnames(all_AUC) <- c("AUC","task","kernel","trend","testData","Iteration","randSampling","initProp")


whichProp <- data.frame(matrix(c(NA,0,0.05,0.1,0.15),nrow=1))
colnames(whichProp) <- seq(0,4)


k=1
i=1
for(tsk_num in all_tsk){
  
  # Scenario1: training set with BER instances above threshold 
  fullfoldPl <- paste(folderPlots,"tsk_",tsk_num,"/",sep="")
  fin_it <- tsk_fin[i]
  cat("SCENARIO 1\ntsk_num",tsk_num,"file",paste(fullfoldPl,"AUCres",fin_it,".RData",sep=""))
  
  load(paste(fullfoldPl,"AUCres",fin_it,".RData",sep=""))
  
  all_AUC[k-1 + 1:sizeAUC,-c(1,6,8)]<-matrix(c(tsk_num,all_exp1$whichCov[tsk_num],all_exp1$whichTrend[tsk_num],1,0),ncol=5,nrow=sizeAUC,byrow = T)
  all_AUC[k-1 + 1:sizeAUC,6]<-1:sizeAUC
  for(j in seq(fin_it+1)){
    all_AUC[k+j-1,1] <- AUC_td[j]
  }
  #  cat("whichProp:",as.numeric(whichProp[as.character(all_exp1[tsk_num,6])]))
  all_AUC[k-1 + 1:sizeAUC,8]<-rep(as.numeric(whichProp[as.character(all_exp1[tsk_num,6])]),sizeAUC)
  k<-k+sizeAUC
  
  
  all_AUC[k-1 + 1:sizeAUC,-c(1,6,8)]<-matrix(c(tsk_num,all_exp1$whichCov[tsk_num],all_exp1$whichTrend[tsk_num],2,0),ncol=5,nrow=sizeAUC,byrow = T)
  all_AUC[k-1 + 1:sizeAUC,6]<-1:sizeAUC
  for(j in seq(fin_it+1)){
    all_AUC[k+j-1,1] <- AUC_sel[j]
  }
  all_AUC[k-1 + 1:sizeAUC,8]<-rep(as.numeric(whichProp[as.character(all_exp1[tsk_num,6])]),sizeAUC)
  k<-k+sizeAUC
  
  all_AUC[k-1 + 1:sizeAUC,-c(1,6,8)]<-matrix(c(tsk_num,all_exp1$whichCov[tsk_num],all_exp1$whichTrend[tsk_num],1,1),ncol=5,nrow=sizeAUC,byrow = T)
  all_AUC[k-1 + 1:sizeAUC,6]<-1:sizeAUC
  for(j in seq(length(AUC_plus_td))){
    if(j==1){
      all_AUC[k+j-1,1] <- AUC_plus_td[j]
    }else{
      all_AUC[k+1 + ((j-2)*res_rand_auc):((j-1)*res_rand_auc-1),1] <- rep(AUC_plus_td[j],res_rand_auc)
    }
  }
  all_AUC[k-1 + 1:sizeAUC,8]<-rep(as.numeric(whichProp[as.character(all_exp1[tsk_num,6])]),sizeAUC)
  k<-k+sizeAUC
  
  all_AUC[k-1 + 1:sizeAUC,-c(1,6,8)]<-matrix(c(tsk_num,all_exp1$whichCov[tsk_num],all_exp1$whichTrend[tsk_num],2,1),ncol=5,nrow=sizeAUC,byrow = T)
  all_AUC[k-1 + 1:sizeAUC,6]<-1:sizeAUC
  for(j in seq(length(AUC_plus_td))){
    if(j==1){
      all_AUC[k+j-1,1] <- AUC_plus_sel[j]
    }else{
      all_AUC[k+1 + ((j-2)*res_rand_auc):((j-1)*res_rand_auc-1),1] <- rep(AUC_plus_sel[j],res_rand_auc)
    }
  }
  all_AUC[k-1 + 1:sizeAUC,8]<-rep(as.numeric(whichProp[as.character(all_exp1[tsk_num,6])]),sizeAUC)
  k<-k+sizeAUC
  cat("DONE\n")
  
  i=i+1
}

fin_rand_AUC <- data.frame(matrix(NA,ncol=6,nrow=length(all_tsk)*(2)))
sd_fin_rand_AUC <- data.frame(matrix(NA,ncol=6,nrow=length(all_tsk)*(2)))
q_up_fin_rand_AUC <- data.frame(matrix(NA,ncol=6,nrow=length(all_tsk)*(2)))
q_lo_fin_rand_AUC <- data.frame(matrix(NA,ncol=6,nrow=length(all_tsk)*(2)))
##up and lo take the quantiles
up=0.95
lo=0.05

colnames(fin_rand_AUC) <- c("AUC","task","kernel","trend","testData","initProp")
colnames(sd_fin_rand_AUC) <- c("AUC","task","kernel","trend","testData","initProp")
colnames(q_up_fin_rand_AUC) <- c("AUC","task","kernel","trend","testData","initProp")
colnames(q_lo_fin_rand_AUC) <- c("AUC","task","kernel","trend","testData","initProp")


k=1
i=1
for(tsk_num in all_tsk){
  
  # Scenario1: training set with BER instances above threshold 
  fullfoldPl <- paste(folderPlots,"tsk_",tsk_num,"/",sep="")
  
  fin_it <- tsk_fin[i]
  cat("SCENARIO 1\ntsk_num",tsk_num,"file",paste(fullfoldPl,"AUCrand",fin_it,".RData",sep=""))
  load(paste(fullfoldPl,"AUCres",fin_it,".RData",sep=""))
  load(paste(fullfoldPl,"AUCrand",fin_it,".RData",sep=""))
  AUC_plus_s_rnd <- c(AUC_plus_sel[fin_it_rand],AUC_plus_s_rnd)
  AUC_plus_t_rnd <- c(AUC_plus_td[fin_it_rand],AUC_plus_t_rnd)
  
  fin_rand_AUC[k,-6] <- matrix(c(mean(AUC_plus_t_rnd),tsk_num,all_exp1$whichCov[tsk_num],all_exp1$whichTrend[tsk_num],1),ncol=5,nrow=1,byrow = T)
  fin_rand_AUC[k,6]<-as.numeric(whichProp[as.character(all_exp1[tsk_num,6])])
  
  sd_fin_rand_AUC[k,-6] <- matrix(c(sd(AUC_plus_t_rnd),tsk_num,all_exp1$whichCov[tsk_num],all_exp1$whichTrend[tsk_num],1),ncol=5,nrow=1,byrow = T)
  sd_fin_rand_AUC[k,6] <-as.numeric(whichProp[as.character(all_exp1[tsk_num,6])])
  
  q_up_fin_rand_AUC[k,-6] <- matrix(c(quantile(AUC_plus_t_rnd,up),tsk_num,all_exp1$whichCov[tsk_num],all_exp1$whichTrend[tsk_num],1),ncol=5,nrow=1,byrow = T)
  q_up_fin_rand_AUC[k,6] <-as.numeric(whichProp[as.character(all_exp1[tsk_num,6])])
  
  q_lo_fin_rand_AUC[k,-6] <- matrix(c(quantile(AUC_plus_t_rnd,lo),tsk_num,all_exp1$whichCov[tsk_num],all_exp1$whichTrend[tsk_num],1),ncol=5,nrow=1,byrow = T)
  q_lo_fin_rand_AUC[k,6] <-as.numeric(whichProp[as.character(all_exp1[tsk_num,6])])
  
  k=k+1
  fin_rand_AUC[k,-6] <- matrix(c(mean(AUC_plus_s_rnd),tsk_num,all_exp1$whichCov[tsk_num],all_exp1$whichTrend[tsk_num],2),ncol=5,nrow=1,byrow = T)
  fin_rand_AUC[k,6]<-as.numeric(whichProp[as.character(all_exp1[tsk_num,6])])
  
  sd_fin_rand_AUC[k,-6] <- matrix(c(sd(AUC_plus_s_rnd),tsk_num,all_exp1$whichCov[tsk_num],all_exp1$whichTrend[tsk_num],2),ncol=5,nrow=1,byrow = T)
  sd_fin_rand_AUC[k,6] <-as.numeric(whichProp[as.character(all_exp1[tsk_num,6])])
  
  
  q_up_fin_rand_AUC[k,-6] <- matrix(c(quantile(AUC_plus_s_rnd,up),tsk_num,all_exp1$whichCov[tsk_num],all_exp1$whichTrend[tsk_num],2),ncol=5,nrow=1,byrow = T)
  q_up_fin_rand_AUC[k,6] <-as.numeric(whichProp[as.character(all_exp1[tsk_num,6])])
  
  q_lo_fin_rand_AUC[k,-6] <- matrix(c(quantile(AUC_plus_s_rnd,lo),tsk_num,all_exp1$whichCov[tsk_num],all_exp1$whichTrend[tsk_num],2),ncol=5,nrow=1,byrow = T)
  q_lo_fin_rand_AUC[k,6] <-as.numeric(whichProp[as.character(all_exp1[tsk_num,6])])
  
  
  k=k+1
  cat("DONE\n")
  
  i=i+1
}


##########################
## Reduce the size of the data frame
##########################S
newAUC <- all_AUC[,c(1,3,5,6,7,8)]
newAUC<-na.omit(newAUC)
newAUC[,2] <- factor(newAUC[,2],labels = c("Mat32","Mat52","SE"))
newAUC[,3] <- factor(newAUC[,3],labels = c("t1","t2"))
newAUC[,5] <- factor(newAUC[,5],labels = c("SUR","random"))

colnames(newAUC)[5]<-"Sampling"
colnames(newAUC)[6]<-"Scenario"
colnames(newAUC)[3]<-"TestData"






##########################
### mean and sd of all experiments with random samples
##########################
new_fin <- fin_rand_AUC[,c(1,3,5,6)]
new_fin <- na.omit(new_fin)
new_fin[,2] <- factor(new_fin[,2],labels = c("Mat32","Mat52","SE"))
new_fin[,3] <- factor(new_fin[,3],labels = c("t1","t2"))
colnames(new_fin)[3]<-"TestData"
colnames(new_fin)[4]<-"Scenario"


new_fin_sd <- sd_fin_rand_AUC[,c(1,3,5,6)]
new_fin_sd <- na.omit(new_fin_sd)
new_fin_sd[,2] <- factor(new_fin_sd[,2],labels = c("Mat32","Mat52","SE"))
new_fin_sd[,3] <- factor(new_fin_sd[,3],labels = c("t1","t2"))
colnames(new_fin_sd)[3]<-"TestData"
colnames(new_fin_sd)[4]<-"Scenario"

new_fin_q_up <- q_up_fin_rand_AUC[,c(1,3,5,6)]
new_fin_q_up <- na.omit(new_fin_q_up)
new_fin_q_up[,2] <- factor(new_fin_q_up[,2],labels = c("Mat32","Mat52","SE"))
new_fin_q_up[,3] <- factor(new_fin_q_up[,3],labels = c("t1","t2"))
colnames(new_fin_q_up)[3]<-"TestData"
colnames(new_fin_q_up)[4]<-"Scenario"

new_fin_q_lo <- q_lo_fin_rand_AUC[,c(1,3,5,6)]
new_fin_q_lo <- na.omit(new_fin_q_lo)
new_fin_q_lo[,2] <- factor(new_fin_q_lo[,2],labels = c("Mat32","Mat52","SE"))
new_fin_q_lo[,3] <- factor(new_fin_q_lo[,3],labels = c("t1","t2"))
colnames(new_fin_q_lo)[3]<-"TestData"
colnames(new_fin_q_lo)[4]<-"Scenario"

##########################
##### Here we plot TestData==t1
##########################
nA_sc1<- filter(newAUC,TestData=="t1")
nA_sc1<- filter(nA_sc1,Scenario<0.15)

nF_sc1<- filter(new_fin,TestData=="t1")
nF_sc1<- filter(nF_sc1,Scenario<0.15)

# option CI
nF_l_sc1 <- cbind(new_fin,new_fin[,1]+1.65*new_fin_sd[,1])
nF_l_sc1[,1] <-new_fin[,1]-1.65*new_fin_sd[,1]
# option quantiles
nF_l_sc1 <- cbind(new_fin,new_fin_q_up[,1])
nF_l_sc1[,1] <-new_fin_q_lo[,1]

colnames(nF_l_sc1)[5] <- "AUCu"
colnames(nF_l_sc1)[1] <- "AUCl"

nF_l_sc1<- filter(nF_l_sc1,TestData=="t1")
nF_l_sc1<- filter(nF_l_sc1,Scenario<0.15)


nA_sc1<- filter(nA_sc1,Sampling==c("SUR"))

## This is the common theme for plots in the paper
Scenario_names = c("0"="tau=0","0.05"="tau=5%","0.1"="tau=10%")

commonGGtheme<- theme_bw() + 
  theme(axis.title.x = element_text(size=30), axis.text.x=element_text(size=26),axis.text.y=element_text(size=26),axis.title.y = element_text(size=30),plot.title=element_text(size=36),legend.text=element_text(size=28),legend.title=element_text(size=30),legend.key.size=unit(1,"cm"),strip.text = element_text(size=25))+
  theme(legend.position="top")

gg<- ggplot(na.omit(nA_sc1),aes(x=Iteration,y=AUC))+
  #geom_smooth(size=0.8,aes(linetype=kernel,colour=kernel),method="loess",se=FALSE,span=0.1)+
  geom_line(size=1.5,aes(linetype=kernel,colour=kernel),na.rm = TRUE)+
  facet_grid(~Scenario, labeller = as_labeller(Scenario_names)) +
  ylim(0.975,0.995)+
  geom_hline(aes(yintercept = AUC,linetype=kernel),data = nF_sc1)+
  commonGGtheme +
  geom_ribbon(aes(ymin=AUCl,ymax=AUCu,colour=kernel),data=merge(nA_sc1,nF_l_sc1),alpha=0.2)#+ geom_hline(aes(yintercept=AUC, linetype=kernel),data=nF_l_sc1) + geom_hline(aes(yintercept=AUCu, linetype=kernel),data=nF_l_sc1)


print(gg)


nA_sc1 %>% group_by(Scenario,kernel) %>% summarize(max(AUC))


##########################
## Test data t2
##########################
nA_sc2<- filter(newAUC,TestData=="t2")
nF_sc2<- filter(new_fin,TestData=="t2")

nA_sc2<- filter(nA_sc2,Sampling==c("SUR"))

# option CI
nF_l_sc2 <- cbind(new_fin,new_fin[,1]+1.65*new_fin_sd[,1])
nF_l_sc2[,1] <-new_fin[,1]-1.65*new_fin_sd[,1]
# option quantiles
nF_l_sc2 <- cbind(new_fin,new_fin_q_up[,1])
nF_l_sc2[,1] <-new_fin_q_lo[,1]

colnames(nF_l_sc2)[5] <- "AUCu"
colnames(nF_l_sc2)[1] <- "AUCl"

nF_l_sc2<- filter(nF_l_sc2,TestData=="t2")



gg<- ggplot(na.omit(nA_sc2),aes(x=Iteration,y=AUC))+
  #geom_smooth(size=0.8,aes(linetype=kernel,colour=kernel),method="loess",se=FALSE,span=0.1)+
  geom_line(size=1,aes(linetype=kernel,colour=kernel),na.rm = TRUE)+
  facet_grid(~Scenario, labeller = as_labeller(Scenario_names)) +
  ylim(0.9,0.98)+
  geom_hline(aes(yintercept = AUC,linetype=kernel),data = nF_sc2)+
  commonGGtheme +
  geom_ribbon(aes(ymin=AUCl,ymax=AUCu,colour=kernel),data=merge(nA_sc2,nF_l_sc2),alpha=0.2)#+ geom_hline(aes(yintercept=AUC, linetype=kernel),data=nF_l_sc2) + geom_hline(aes(yintercept=AUCu, linetype=kernel),data=nF_l_sc2)



print(gg)



##########################
### Table to compare AUC values between test data
##########################

compareAUC<- newAUC %>% filter(Scenario<0.15) %>% group_by(Scenario,TestData,kernel) %>% summarize(AUC=round(max(AUC),4))
t(spread(compareAUC,kernel,AUC))

##########################
### Compute when AL is better than average random
##########################
AL_betterRand <- data.frame(matrix(NA,ncol=4,nrow= 2*length(unique(newAUC$Scenario))*length(unique(newAUC$kernel))))
colnames(AL_betterRand) <- c("IterationEqual","kernel","testData","Scenario")

i=1
for(kk in unique(newAUC$kernel)){
  for(ss in unique(newAUC$Scenario)){
    for(td in unique(newAUC$TestData)){
      ff= filter(newAUC,TestData==td & Scenario==ss & kernel==kk & Sampling =="SUR")
      val <- filter(new_fin,kernel == kk & TestData==td & Scenario==ss)$AUC
      AL_betterRand[i,1]<- length(ff$AUC)-min(which(ff$AUC>val))
      AL_betterRand[i,-1] <- ff[1,-c(1,4,5)]
      i=i+1
    }
  }
}

AL_betterRand[,2] <- factor(AL_betterRand[,2],labels = c("Mat32","Mat52","SE"))
gg<- ggplot(na.omit(AL_betterRand),aes(x=Scenario,y=IterationEqual))+
  geom_line(size=0.8,aes(linetype=kernel,colour=kernel),na.rm = TRUE)+
  geom_point(aes(colour=kernel),na.rm = TRUE)+
  facet_grid(~testData) 
print(gg)

##################################
### AL better than 1.65*sd
##################################
AL_betterRand90 <- data.frame(matrix(NA,ncol=4,nrow= 2*length(unique(newAUC$Scenario))*length(unique(newAUC$kernel))))
colnames(AL_betterRand90) <- c("IterationEqual","kernel","testData","Scenario")

i=1
for(kk in unique(newAUC$kernel)){
  for(ss in unique(newAUC$Scenario)){
    for(td in unique(newAUC$TestData)){
      ff= filter(newAUC,TestData==td & Scenario==ss & kernel==kk & Sampling =="SUR")
      val <- filter(new_fin,kernel == kk & TestData==td & Scenario==ss)$AUC
      val <- val+1.65*filter(new_fin_sd,kernel == kk & TestData==td & Scenario==ss)$AUC
      AL_betterRand90[i,1]<- length(ff$AUC)-min(which(ff$AUC>val))
      AL_betterRand90[i,-1] <- ff[1,-c(1,4,5)]
      
      plot(ff$AUC,type='l',ylim=c(min(val,quantile(ff$AUC,0.5)),0.999),main=paste(kk,ss,td))
      abline(h=val)
      abline(v=min(which(ff$AUC>val)))
      
      i=i+1
    }
  }
}

AL_betterRand90[,2] <- factor(AL_betterRand90[,2],labels = c("Mat32","Mat52","SE"))
gg<- ggplot(na.omit(AL_betterRand90),aes(x=Scenario,y=IterationEqual))+
  geom_line(size=0.8,aes(linetype=kernel,colour=kernel),na.rm = TRUE)+
  geom_point(aes(colour=kernel),na.rm = TRUE)+
  facet_grid(~testData) 
  
print(gg)

filter(AL_betterRand90,testData==1)


AL_betterRand90_t1 <- filter(AL_betterRand90,Scenario<0.15,testData==1)
gg<- ggplot(na.omit(AL_betterRand90_t1),aes(x=Scenario,y=IterationEqual))+
  geom_line(size=1,aes(linetype=kernel,colour=kernel),na.rm = TRUE)+
  geom_point(size=3,aes(colour=kernel,shape=kernel),na.rm = TRUE)+ 
  ylab("Iteration saved") +xlab("Tau")+
  commonGGtheme

print(gg)


## Compute number of BER values above T saved by AL
job_num<- all_tsk
folderResults <- './Results/'
tt_threshold <- log10(4e-3)


whichCov <- c("Mat32","Mat52","SE")
names(whichCov) <- seq(1,3)

aboveTsaved <- data.frame(matrix(NA,ncol=5,nrow=nrow(AL_betterRand90)))
colnames(aboveTsaved) <- c("TotRandSampl","TotAL",colnames(AL_betterRand90)[-c(1)])

i=1
k=1
for(tsk_num in all_tsk){
  
  # Scenario1: training set with BER instances above threshold 
  fullFoldRes <- paste(folderResults,"job_",job_num[k],"/",sep="")
  
  fullFileToLoad <-list.files(path = fullFoldRes,pattern=glob2rx(paste("exp",tsk_num,"_n0_",all_exp1$n0[tsk_num],"it",all_exp1$n_iter[tsk_num]+1,"_surCrit_*",sep="")),full.names = TRUE)
  
  if(length(fullFileToLoad)>0)
    load(fullFileToLoad)
  
  ## Test data t1
  itSav <- filter(AL_betterRand90,Scenario==as.numeric(whichProp[as.character(exp_params$whichTraining)]),kernel==whichCov[exp_params$whichCov],testData==1)$IterationEqual
           
  aboveTsaved[i,1] <- sum(output>tt_threshold)
  aboveTsaved[i,2] <- sum(output[1:(exp_params$n_iter-itSav+exp_params$n0)]>tt_threshold)
  
  aboveTsaved[i,3] <- whichCov[exp_params$whichCov]
  aboveTsaved[i,4] <- 1
  aboveTsaved[i,5] <- as.numeric(whichProp[as.character(exp_params$whichTraining)])

  i=i+1
  
  ## Test data t2
  itSav <- filter(AL_betterRand90,Scenario==as.numeric(whichProp[as.character(exp_params$whichTraining)]),kernel==whichCov[exp_params$whichCov],testData==2)$IterationEqual
  
  aboveTsaved[i,1] <- sum(output>tt_threshold)
  aboveTsaved[i,2] <- sum(output[1:(exp_params$n_iter-itSav+exp_params$n0)]>tt_threshold)
  
  aboveTsaved[i,3] <- whichCov[exp_params$whichCov]
  aboveTsaved[i,4] <- 2
  aboveTsaved[i,5] <- as.numeric(whichProp[as.character(exp_params$whichTraining)])
  
  i=i+1
  
  k=k+1
  
}


aboveTsaved<-filter(aboveTsaved, Scenario<0.15)


# Table with absolute values of randSampling-AL
aboveTsaved_absVals <- mutate(aboveTsaved, InstSaved=TotRandSampl-TotAL)
## test data t1
aboveTsaved_absVals_t1 <- filter(aboveTsaved_absVals, testData==1)
t(spread(aboveTsaved_absVals_t1[,c(3,5,6)],kernel,InstSaved))


# Table with proportion AL/randSampling
aboveTsaved <- mutate(aboveTsaved, proportionALoverRand=TotAL/TotRandSampl)
## test data 1
aboveTsaved_t1 <- filter(aboveTsaved, testData==1)
abTsaved_table_t1 <- aboveTsaved_t1[,c(3,5,6)]
round(t(spread(abTsaved_table_t1,kernel,proportionALoverRand)),4)*100



## Test data 1
AL_betterRand90_t1_table <- AL_betterRand90_t1
AL_betterRand90_t1_table[,3] <- NULL
# table with actual number of iteration saved
t(spread(AL_betterRand90_t1_table,kernel,IterationEqual))

## table with proportion of number of iteration in AL vs rand sampling
AL_betterRand90_t1_table<-mutate(AL_betterRand90_t1_table,prop=(401-IterationEqual)/(500))
AL_betterRand90_t1_table[,1] <- NULL
round(t(spread(AL_betterRand90_t1_table,kernel,prop)),3)*100




## Test data 2
AL_betterRand90_t2_table <- filter(AL_betterRand90,testData==2)
AL_betterRand90_t2_table[,3] <- NULL
# table with actual number of iteration saved
t(spread(AL_betterRand90_t2_table,kernel,IterationEqual))

aboveTsaved_absVals_t2 <- filter(aboveTsaved_absVals, testData==2)
t(spread(aboveTsaved_absVals_t2[,c(3,5,6)],kernel,InstSaved))


# Table with proportion AL/randSampling
aboveTsaved_t2 <- filter(aboveTsaved, testData==2)
abTsaved_table_t2 <- aboveTsaved_t2[,c(3,5,6)]
round(t(spread(abTsaved_table_t2,kernel,proportionALoverRand)),4)*100

## table with proportion of number of iteration in AL vs rand sampling
AL_betterRand90_t2_table<-mutate(AL_betterRand90_t2_table,prop=(401-IterationEqual)/(500))
AL_betterRand90_t2_table <- filter(AL_betterRand90_t2_table,Scenario<0.15)
AL_betterRand90_t2_table[,1] <- NULL
round(t(spread(AL_betterRand90_t2_table,kernel,prop)),3)*100

